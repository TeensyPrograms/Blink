This Blink program was used as a test to setup Qt to be used as an editor for
Teensy programs.  See the Qt project for details on using Qt for editing,
making, and uploading to Teensy MCUs.

Notes:

    1.  Currently this only handles Teensy 3.2.  I plan on changing this so that 
        the parameters can be entered and the Makefile created with them.
    3.  The main.cpp is a sample from one of Paul's repositories.
    4.  Tools, libraries, and other files were copied from the Arduino
        directory after the Teensyduino was installed.  Later they will be moved 
        to a directory for use by all projects.


Setup:

The BlinkLED project directory has the following structure.  My main directory
for Teensy projects is D:/Projects/TeensyProjects and BlinkLED was put in the 
BlinkLED directory under TeensyProjects.

BlinkLED - main, home project directory
    Build - used to build the project.  the .hex, .0, and other build files end
        up here
    *libraries - contains the Teensy libraries.  Not used yet
    src - Put the *.c and *.cpp, files in here.  main.cpp resides here.
    *teensy3 - contains the files from 
        <installlocation>\Arduino\hardware\teensy\avr\cores\teensy3
    *tools - the teensy tools
    
Directories marked with a * will be moved to a TeensyCommon directory so they
don not have to be included in every project but to keep things simple during 
testing they were put in the BlinkLED directory.

Installation - Files

First the files have to be installed to a directory of your choice.

    1.  Download the BlinkLED directory to your computer.
    2.  Open Makefile in a text editor.
        * The User Defined Variables section is between the comment lines 
          consisting of the = sign are variables that need to be set for your
          installation.  Nothing after them should need to be set as everything
          else is dervived from them.
    3.  Save the file and build.
    
